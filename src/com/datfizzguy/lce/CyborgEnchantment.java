package com.datfizzguy.lce;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.rit.sucy.CustomEnchantment;

public class CyborgEnchantment extends CustomEnchantment
{
	static final Material[] CYBORG_ITEMS = new Material[] {
		Material.IRON_HELMET, Material.GOLD_HELMET, Material.DIAMOND_HELMET 
		};
	public CyborgEnchantment() {
		super("Cyborg", CYBORG_ITEMS);
		description = "Provides Night Vision, Speed, and Jump Boost when worn.";
		this.max = 3;
		this.base = 15;
		this.interval = 5;
	}
	@Override
	public void applyEquipEffect(Player player, int enchantlevel) 
	{
		int effect = enchantlevel + 1;
		player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 100000, effect));
	}
		public void applyUnequipEffect(Player player, int enchantlevel)
		{
			player.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
}